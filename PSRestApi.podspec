Pod::Spec.new do |s|
  s.name             = "PSRestApi"
  s.version          = "1.5.1"
  s.summary          = "My own simple library for rest api"
  s.homepage         = "https://bitbucket.org/pavelsmelovsky/psrestapi/overview"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Pavel Smelovsky" => "pavel.smelovsky@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/pavelsmelovsky/psrestapi.git", :tag => "v#{s.version}" }
  s.social_media_url = 'https://twitter.com/PavelSmelovsky'
  s.requires_arc = true

  s.swift_version       = "5.0"
  s.pod_target_xcconfig = { "SWIFT_VERSION" => "5.0" }

  # deployment target
  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.12'

  # dependent frameworks
  s.framework    = 'Foundation'

  s.module_name   = 'PSRestApi'
  s.source_files  = 'PSRestApi/PSRestApi/**/*.{swift}'

end
