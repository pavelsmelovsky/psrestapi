//
//  RequestHeadersContainer.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 21.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol RequestHeadersContainer: class {
    var headers: [String: Any] { get set }
}

public extension RequestHeadersContainer {
    func set(value: String?, forHttpHeaderField name: String) {
        if let value = value {
            headers[name] = value
        } else {
            headers.removeValue(forKey: name)
        }
    }

    func add(value: String, forHttpHeaderField name: String) {
        let existingValue = headers[name]
        if var array = existingValue as? [String] {
            array.append(value)
            headers[name] = array
        } else if let str = existingValue as? String {
            headers[name] = [str, value]
        } else {
            headers[name] = value
        }
    }

    func value(forHttpHeaderField name: String) -> String? {
        let obj = headers[name]
        if let str = obj as? String {
            return str
        } else if let values = obj as? [String] {
            return values.joined(separator: ",")
        }
        return nil
    }
}
