//
//  ApiTaskApiSessionDelegate.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 02.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

internal protocol ApiTaskApiSessionDelegate: class {
    func apiSession(_ apiSession: ApiSession, didReceiveResponse response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Swift.Void)
    func apiSession(_ apiSession: ApiSession, didReceiveData data: Data)

    func apiSessionLoadingCompleted(_ apiSession: ApiSession) throws
    func apiSession(_ apiSession: ApiSession, didCompleteWithError error: Error?)
}
