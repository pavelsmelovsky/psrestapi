//
//  RestApiSession+ApiSessionTasks.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

extension RestApiSession: ApiSessionTasks {
    // MARK: - Internal methods

    internal func allTasks() -> [ApiTaskInternal] {
        return self.tasks
    }

    internal func task(withSessionTask sessionTask: URLSessionTask) -> ApiTaskInternal? {
        return self.tasks.first(where: {$0.task == sessionTask})
    }

    internal func removeTask(withSessionTask sessionTask: URLSessionTask) -> ApiTaskInternal? {
        guard let index = self.tasks.firstIndex(where: {$0.task == sessionTask}) else {
            return nil
        }

        return self.tasks.remove(at: index)
    }
}
