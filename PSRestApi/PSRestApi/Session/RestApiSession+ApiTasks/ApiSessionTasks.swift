//
//  ApiSessionTasks.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

internal protocol ApiSessionTasks: ApiSession {
    func allTasks() -> [ApiTaskInternal]
    func task(withSessionTask sessionTask: URLSessionTask) -> ApiTaskInternal?
    func removeTask(withSessionTask sessionTask: URLSessionTask) -> ApiTaskInternal?
}
