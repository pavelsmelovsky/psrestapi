//
//  RestApiSession+Rest.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 03.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

private let kVoidTransformer = VoidTransformer()

public extension RestApiSession {
    func create<Req: Transformer, Res: Transformer>(endpoint: String, data: Req.InputType, requestTransformer: Req, responseTransformer: Res) -> ApiTask<Req, Res> {
        let task = self.apiTask(endpoint: endpoint, method: .post, requestTransformer: requestTransformer, responseTransformer: responseTransformer)
        self.set(data: data, forTask: task)
        return task
    }

    func create<Res: Transformer>(endpoint: String, responseTransformer: Res) -> ApiTask<VoidTransformer, Res> {
        return self.apiTask(endpoint: endpoint, method: .post, requestTransformer: kVoidTransformer, responseTransformer: responseTransformer)
    }

    func read<Res: Transformer>(endpoint: String, responseTransformer: Res) -> ApiTask<VoidTransformer, Res> {
        return self.apiTask(endpoint: endpoint, method: .get, requestTransformer: kVoidTransformer, responseTransformer: responseTransformer)
    }

    func update<Req: Transformer, Res: Transformer>(endpoint: String, data: Req.InputType, requestTransformer: Req, responseTransformer: Res) -> ApiTask<Req, Res> {
        let task = self.apiTask(endpoint: endpoint, method: .put, requestTransformer: requestTransformer, responseTransformer: responseTransformer)
        self.set(data: data, forTask: task)
        return task
    }

    func patch<Req: Transformer, Res: Transformer>(endpoint: String, data: Req.InputType, requestTransformer: Req, responseTransformer: Res) -> ApiTask<Req, Res> {
        let task = self.apiTask(endpoint: endpoint, method: .patch, requestTransformer: requestTransformer, responseTransformer: responseTransformer)
        self.set(data: data, forTask: task)
        return task
    }

    func delete<Res: Transformer>(endpoint: String, responseTransformer: Res) -> ApiTask<VoidTransformer, Res> {
        return self.apiTask(endpoint: endpoint, method: .delete, requestTransformer: kVoidTransformer, responseTransformer: responseTransformer)
    }

    private func apiTask<Req: Transformer, Res: Transformer>(endpoint: String, method: ApiTaskMethod, requestTransformer: Req, responseTransformer: Res) -> ApiTaskInternalImpl<Req, Res> {
        let apiTask = ApiTaskInternalImpl<Req, Res>(delegate: self, endpoint: endpoint, httpMethod: method, requestTransformer: requestTransformer, responseTransformer: responseTransformer)
        return apiTask
    }

    private func set<Req: Transformer, Res: Transformer>(data: Req.InputType, forTask task: ApiTaskInternalImpl<Req, Res>) {
        task.bodyData = data
    }
}
