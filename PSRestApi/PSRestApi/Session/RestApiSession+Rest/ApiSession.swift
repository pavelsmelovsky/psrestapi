//
//  ApiSession.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 03.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol ApiSession: RequestHeadersContainer {
    var url: URL { get }

    func register(middleware: ApiTaskMiddleware)

    func create<Res: Transformer>(endpoint: String, responseTransformer: Res) -> ApiTask<VoidTransformer, Res>
    func create<Req: Transformer, Res: Transformer>(endpoint: String, data: Req.InputType, requestTransformer: Req, responseTransformer: Res) -> ApiTask<Req, Res>

    func read<Res: Transformer>(endpoint: String, responseTransformer: Res) -> ApiTask<VoidTransformer, Res>

    func update<Req: Transformer, Res: Transformer>(endpoint: String, data: Req.InputType, requestTransformer: Req, responseTransformer: Res) -> ApiTask<Req, Res>

    func patch<Req: Transformer, Res: Transformer>(endpoint: String, data: Req.InputType, requestTransformer: Req, responseTransformer: Res) -> ApiTask<Req, Res>

    func delete<Res: Transformer>(endpoint: String, responseTransformer: Res) -> ApiTask<VoidTransformer, Res>
}
