//
//  RestApiSession+URLSession.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.02.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

extension RestApiSession {
    func urlSession<T: Session>(_ conf: URLSessionConfiguration?) -> T {
        let conf = conf ?? self.defaultSessionConfiguration()
        return T.init(configuration: conf, delegate: self, delegateQueue: nil)
    }

    func defaultSessionConfiguration() -> URLSessionConfiguration {
        let conf = URLSessionConfiguration.default
        conf.requestCachePolicy = .useProtocolCachePolicy
        conf.allowsCellularAccess = true
        return conf
    }
}
