//
//  RestApiSession+DataTask.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 21.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

extension RestApiSession: URLSessionDataDelegate {
    // MARK: - URLSession delegate

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Swift.Void) {
        logVerbose("urlSession(_:dataTask:didReceiveResponse:completionHandler)")

        if let apiTask = self.task(withSessionTask: dataTask) {
            apiTask.apiSession(self, didReceiveResponse: response, completionHandler: completionHandler)
        } else {
            completionHandler(.cancel)
        }
    }

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        logVerbose("urlSession(_:dataTask:didBecomeDownloadTask:)")
    }

    @available(iOS 9.0, *)
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        logVerbose("urlSession(_:dataTask:didBecomeStreamTask:)")
    }

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        logVerbose("urlSession(_:dataTask:didReceiveData:)")

        guard let apiTask = self.task(withSessionTask: dataTask) else {
            preconditionFailure("Api request not found for \(dataTask.taskIdentifier) task")
        }

        apiTask.apiSession(self, didReceiveData: data)
    }

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: @escaping (CachedURLResponse?) -> Swift.Void) {
        logVerbose("urlSession(_:dataTask:willCacheResponse:completionhandler:)")
        completionHandler(proposedResponse)
    }
}
