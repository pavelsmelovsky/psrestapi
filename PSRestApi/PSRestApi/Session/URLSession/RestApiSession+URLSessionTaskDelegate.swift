//
//  RestApiSession+General.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 24.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

extension RestApiSession: URLSessionTaskDelegate {
    public func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Swift.Void) {
        completionHandler(request)
    }

//    public func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
//        logVerbose("urlSession(_:task:didReceive:completionHandler)")
//
//        guard let serverTrust = challenge.protectionSpace.serverTrust else {
//            completionHandler(.cancelAuthenticationChallenge, nil)
//            return
//        }
//
//        let count = SecTrustGetCertificateCount(serverTrust)
//        if 0 == count {
//            completionHandler(.cancelAuthenticationChallenge, nil)
//            return
//        }
//
//        guard let certificate = SecTrustGetCertificateAtIndex(serverTrust, 0) else {
//            completionHandler(.cancelAuthenticationChallenge, nil)
//            return
//        }
//
//        var policies = [SecPolicy]()
//
//        let policy = SecPolicyCreateSSL(true, challenge.protectionSpace.host as CFString)
//        policies.append(policy)
//
//        SecTrustSetPolicies(serverTrust, policies as CFTypeRef)
//
//        var result: SecTrustResultType = .unspecified
//        let status = withUnsafeMutablePointer(to: &result) { (pointer) -> OSStatus in
//            return SecTrustEvaluate(serverTrust, pointer)
//        }
//
//        let serverTrusted = result == .proceed || result == .unspecified
//
//        let remoteCertData = SecCertificateCopyData(certificate)
//
//        // compare remote cert data with local and return success if equal
//        if serverTrusted/* && remoteCertData == localCertData*/ {
//            let credentials = URLCredential(trust: serverTrust)
//            completionHandler(.useCredential, credentials)
//        } else {
//            completionHandler(.cancelAuthenticationChallenge, nil)
//        }
//    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, needNewBodyStream completionHandler: @escaping (InputStream?) -> Swift.Void) {
        logVerbose("urlSession(_:task:needNewBodyStreem:completionHandler)")
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        guard let apiTask = self.task(withSessionTask: task) else {
            preconditionFailure("Cannot find api request for task \(task.taskIdentifier)")
        }

        if totalBytesExpectedToSend != NSURLSessionTransferSizeUnknown && totalBytesSent > 0 {
            let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
            apiTask.uploadProgress = progress
        }

        logVerbose("urlSession(_:task:didSendBodyData:bytesSent:totalBytesSent:totalBytesExpectedToSend)")
    }

    @available(iOS 10, *)
    public func urlSession(_ session: URLSession, task: URLSessionTask, didFinishCollecting metrics: URLSessionTaskMetrics) {
        logVerbose("urlSession(_:task:didFinishCollecting:)")
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let apiTask = self.removeTask(withSessionTask: task) else {
            fatalError()
        }

        logVerbose("urlSession(_:task:didCompleteWithError)")

        do {
            if let error = error {
                throw error
            }

            if apiTask.response == nil {
                throw NSError.ApiSession.invalidResponse
            }

            try apiTask.apiSessionLoadingCompleted(self)

            self.completionQueue.async { [unowned self] in
                apiTask.apiSession(self, didCompleteWithError: nil)
            }
        } catch {
            self.completionQueue.async { [unowned self] in
                apiTask.apiSession(self, didCompleteWithError: error)
            }
        }
    }
}
