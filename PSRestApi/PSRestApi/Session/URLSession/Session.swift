//
//  Session.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.02.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol Session {
    init(configuration: URLSessionConfiguration)

    init(configuration: URLSessionConfiguration, delegate: URLSessionDelegate?, delegateQueue queue: OperationQueue?)

    var delegateQueue: OperationQueue { get }
    var delegate: URLSessionDelegate? { get }

    var configuration: URLSessionConfiguration { get }

    var sessionDescription: String? { get set }

    func finishTasksAndInvalidate()
    func invalidateAndCancel()
    func reset(completionHandler: @escaping () -> Swift.Void)
    func flush(completionHandler: @escaping () -> Swift.Void)

    func getTasksWithCompletionHandler(_ completionHandler: @escaping ([URLSessionDataTask], [URLSessionUploadTask], [URLSessionDownloadTask]) -> Swift.Void)

    @available(iOS 9.0, *)
    func getAllTasks(completionHandler: @escaping ([URLSessionTask]) -> Swift.Void)

    func dataTask(with request: URLRequest) -> URLSessionDataTask
    func dataTask(with url: URL) -> URLSessionDataTask
    func uploadTask(with request: URLRequest, fromFile fileURL: URL) -> URLSessionUploadTask
    func uploadTask(with request: URLRequest, from bodyData: Data) -> URLSessionUploadTask
    func uploadTask(withStreamedRequest request: URLRequest) -> URLSessionUploadTask
    func downloadTask(with request: URLRequest) -> URLSessionDownloadTask
    func downloadTask(with url: URL) -> URLSessionDownloadTask
    func downloadTask(withResumeData resumeData: Data) -> URLSessionDownloadTask

    @available(iOS 9.0, *)
    func streamTask(withHostName hostname: String, port: Int) -> URLSessionStreamTask

    @available(iOS 9.0, *)
    func streamTask(with service: NetService) -> URLSessionStreamTask
}
