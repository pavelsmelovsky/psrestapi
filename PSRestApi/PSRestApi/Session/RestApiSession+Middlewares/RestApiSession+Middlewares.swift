//
//  RestApiSession+Middlewares.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 08.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

extension URLSession.ResponseDisposition: Error {
    public var localizedDescription: String {
        return "Response disposition: \(self.stringValue)"
    }

    private var stringValue: String {
        switch self {
        case .allow: return ".allow"
        case .becomeDownload: return ".becomeDownload"
        case .becomeStream: return ".becomeStream"
        case .cancel: return ".cancel"
        @unknown default: return ".unknown"
        }
    }
}

extension RestApiSession: ApiTaskInternalDelegate {
    internal func registerApiTask(_ apiTask: ApiTaskInternal, data: Data?) {
        let urlRequest = URLRequest(session: self, apiTask: apiTask, data: data)
        let sessionTask = self.urlSession.dataTask(with: urlRequest)
        apiTask.task = sessionTask
        self.tasks.append(apiTask)
    }

    internal func apiTask(_ apiTask: ApiTaskInternal, resolveResponseMiddlewares completion: @escaping (URLSession.ResponseDisposition) -> Void) {
        guard let response = apiTask.response else {
            completion(.cancel)
            return
        }

        let taskMiddlewares = apiTask.responseMiddlewares

        if apiTask.ignoreGlobalResponseMiddlewares {
            self.resolveMiddlewares(taskMiddlewares, for: response, completion: completion)
        } else {
            var middlewares = self.responseMiddlewares
            middlewares.append(contentsOf: taskMiddlewares)
            self.resolveMiddlewares(middlewares, for: response, completion: completion)
        }
    }

    private func resolveMiddlewares(_ middlewares: [ApiResponseMiddleware], for response: ApiResponse, completion: (URLSession.ResponseDisposition) -> Swift.Void) {
        do {
            try middlewares.forEach { middleware in
                let result = middleware.receive(response: response)
                if result != .allow { throw result }
            }
            completion(.allow)
        } catch let result as URLSession.ResponseDisposition {
            completion(result)
        } catch {
            logError(error)
            completion(.cancel) // unhandled exception
        }
    }

    internal func apiTask(_ apiTask: ApiTaskInternal, resolveMiddlewares completion: @escaping ApiMiddlewareCallback) {
        let middlewares = self.middlewares.filter({$0.shouldPerform(forTask: apiTask)})
        if middlewares.isEmpty {
            completion(nil)
        } else {
            self.setup(middlewares: middlewares, forTask: apiTask, completion: completion)
        }
    }

    private func setup(middlewares: [ApiTaskMiddleware], forTask task: ApiTaskProperties, completion: @escaping ApiMiddlewareCallback) {
        var middlewares = middlewares
        let first = middlewares.removeFirst()

        first.setup(task: task) { [weak self] (error) in
            if case .canceling = task.state {
                let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: nil)
                completion(error)
            } else if case .preparing = task.state {
                if middlewares.isEmpty || error != nil {
                    completion(error)
                } else {
                    self?.setup(middlewares: middlewares, forTask: task, completion: completion)
                }
            } // else if other state is active then completion block is never called
        }
    }
}
