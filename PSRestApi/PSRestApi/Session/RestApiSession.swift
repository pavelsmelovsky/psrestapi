//
//  RestApiSession.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 21.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

private var _sharedInstance: RestApiSession?

public class RestApiSession: NSObject, ApiSession, RequestHeadersContainer {
    public private(set) var url: URL

    private(set) var urlSession: Session!
    private(set) var completionQueue: DispatchQueue

    private(set) var timeOut: TimeInterval = 30

    public var headers = [String: Any]()

    // TODO: Create generic typealias in Swift 4
    internal var middlewares: [ApiTaskMiddleware] = []

    internal var responseMiddlewares: [ApiResponseMiddleware] = []

    internal var tasks = [ApiTaskInternal]()

    /// Convenience initializer to create RestApiSession object
    ///
    /// - Parameters:
    ///   - url: Base URL, must contain trailing slash
    ///   - configuration: Configuration for url session, can be nil
    ///   - queue: queue for completion callbacks
    convenience public init(url: URL, configuration: URLSessionConfiguration? = nil, queue: DispatchQueue = .main) {
        self.init(url: url, sessionClass: URLSession.self, configuration: configuration, queue: queue)
    }

    /// Convenience initializer to create RestApiSession object
    ///
    /// - Parameters:
    ///   - url: Base URL, must contain trailing slash
    ///   - sessionClass – Custom class, that conforms to protocol Session
    ///   - configuration: Configuration for url session, can be nil
    ///   - queue: queue for completion callbacks
    convenience public init<T: Session>(url: URL, sessionClass: T.Type, configuration: URLSessionConfiguration? = nil, queue: DispatchQueue = .main) {
        self.init(url: url, queue: queue)

        let session: T = self.urlSession(configuration)
        urlSession = session
    }

    private init(url: URL, queue: DispatchQueue) {
        self.completionQueue = queue
        self.url = url

        super.init()
    }

    // MARK: - Public methods

    public func register(middleware: ApiTaskMiddleware) {
        self.middlewares.append(middleware)
    }

    public func register(responseMiddleware: ApiResponseMiddleware) {
        self.responseMiddlewares.append(responseMiddleware)
    }
}
