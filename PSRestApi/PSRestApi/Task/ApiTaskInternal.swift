//
//  ApiTaskInternal.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

internal protocol ApiTaskInternal: ApiTaskProperties, ApiTaskApiSessionDelegate, RequestHeadersContainer {
    var task: URLSessionTask? { get set }
    var state: ApiTaskState { get }

    var uploadProgress: Float { get set }
    var downloadProgress: Float { get set }

    var queryItems: [URLQueryItem] { get }

    var responseMiddlewares: [ApiResponseMiddleware] { get }
}
