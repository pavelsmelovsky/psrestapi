//
//  ApiResponse.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 25.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#if os(OSX)
    import Cocoa
#elseif os(iOS)
    import Foundation
#endif

public let apiResponseErrorDomain = "me.onza.error.apiResponse"

public class ApiResponse {
    public private(set) var response: URLResponse

    public private(set) var rawData: Data?

    /// Initialize ApiResponse with URLResponse
    ///
    /// - Parameter : response: URLResponse
    required internal init(response: URLResponse) {
        self.response = response

        self.initializeData()
    }

    internal func appendData(_ data: Data) {
        self.rawData?.append(data)
    }

    private func initializeData() {
        let expectedCapacity = self.response.expectedContentLength
        let capacity = expectedCapacity == -1 ? 1024 : Int(expectedCapacity)

        // if capacity is not zero then create mutable Data object to store data
        if capacity > 0 {
            self.rawData = Data(capacity: capacity)
        }
    }
}
