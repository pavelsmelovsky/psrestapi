//
//  HttpApiResponse.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 06.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public class HttpApiResponse: ApiResponse {
//    override public private(set) var response: URLResponse?
    public var httpResponse: HTTPURLResponse? {
        return self.response as? HTTPURLResponse
    }

    public var statusCode: ApiStatusCode {
        return ApiStatusCode(rawValue: self.httpResponse?.statusCode ?? 0) ?? .unknown
    }

    public var rawStatusCode: Int {
        return self.httpResponse?.statusCode ?? 0
    }

    public var localizedStatusMessage: String? {
        if let response = self.httpResponse {
            return HTTPURLResponse.localizedString(forStatusCode: response.statusCode)
        }
        return nil
    }

    public func header(forName name: String) -> String? {
        return self.httpResponse?.allHeaderFields[name] as? String
    }
}
