//
//  ApiTaskInternalImpl.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 21.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#if os(OSX)
    import Cocoa
#elseif os(iOS)
    import Foundation
#endif

internal class ApiTaskInternalImpl<Req: Transformer, Res: Transformer>: ApiTask<Req, Res>, ApiTaskInternal {
    private unowned var delegate: ApiTaskInternalDelegate

    private var taskStateObserving: NSKeyValueObservation?
    override public internal(set) var task: URLSessionTask? {
        didSet {
            self.taskStateObserving?.invalidate()
            self.taskStateObserving = nil

            if let task = self.task {
                self.taskStateObserving = task.observe(\.state, options: [.old, .new, .initial], changeHandler: { [unowned self] task, _ in
                    if let state = ApiTaskState(sessionState: task.state) {
                        self.state = state
                    }
                })
            }
        }
    }

    override public var state: ApiTaskState {
        didSet {
            logVerbose("ApiTask.state is \(state.rawValue)")
        }
    }

    /// true if current task is registered in ApiSession
    public fileprivate(set) var isRegistered = false

    override public internal(set) var uploadProgress: Float {
        didSet {
            if let handler = self.uploadProgressCallback {
                self.invokeProgressHandler(handler, progress: uploadProgress)
            }
        }
    }

    override public internal(set) var downloadProgress: Float {
        didSet {
            if let handler = self.downloadProgressCallback {
                self.invokeProgressHandler(handler, progress: downloadProgress)
            }
        }
    }

    deinit {
        self.taskStateObserving?.invalidate()
        self.taskStateObserving = nil
    }

    init(delegate: ApiTaskInternalDelegate, endpoint: String, httpMethod: ApiTaskMethod, requestTransformer: Req, responseTransformer: Res) {
        self.delegate = delegate

        let request = ApiRequestImpl(endpoint: endpoint, httpMethod: httpMethod)
        super.init(apiRequest: request, requestTransformer: requestTransformer, responseTransformer: responseTransformer)
    }

    public required init(apiRequest: ApiRequest, requestTransformer: Req?, responseTransformer: Res) {
        fatalError("init(apiRequest:requestTransformer:responseTransformer:) has not been implemented")
    }

    // MARK: - Cancel with error

    fileprivate func cancel(withError error: Error) {
        self.failure?(error, self)
    }

    // MARK: - Internal methods

    func apiSession(_ apiSession: ApiSession, didReceiveResponse response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Swift.Void) {
        self.response = HttpApiResponse(response: response)

        self.delegate.apiTask(self) { (disposition: URLSession.ResponseDisposition) in
            completionHandler(disposition)
        }
    }

    func apiSession(_ apiSession: ApiSession, didReceiveData data: Data) {
        self.response?.appendData(data)
    }

    func apiSessionLoadingCompleted(_ apiSession: ApiSession) throws {
        try self.transformData()
    }

    func apiSession(_ apiSession: ApiSession, didCompleteWithError error: Error?) {
        if let error = error {
            self.onError(error)
        } else {
            self.onSuccess()
        }
    }

    func onSuccess() {
        assertMainThread(function: #function, file: #file, line: #line)

        if let handler = self.success, let response = self.response {
            handler(self.outputData, response)
        }
    }

    func onError(_ error: Error) {
        assertMainThread(function: #function, file: #file, line: #line)

        if let handler = self.failure {
            handler(error, self)
        }
    }

    /// Invoke progress handler in main queue
    ///
    /// - Parameters:
    ///   - handler: Handler to be invoked
    ///   - progress: Current progress
    private func invokeProgressHandler(_ handler: @escaping ApiTaskProgress, progress: Float) {
        let queue = DispatchQueue.main

        queue.async { [weak self] in
            if let strongSelf = self {
                handler(progress, strongSelf)
            }
        }
    }

    // MARK: - Private methods

    private func onResponseSet() {

    }

    // MARK: - Public methods

    override public func resume() {
        guard self.state == .suspended else {
            preconditionFailure("Invalid task state")
        }

        if !self.isRegistered {
            self.state = .preparing
            self.delegate.apiTask(self) { [weak self] (error: Error?) in
                guard let sSelf = self else { return }

                sSelf.onMiddlewareResolved(withError: error)
            }
        } else {
            self.task?.resume()
        }
    }

    public override func suspend() {
        guard self.state == .preparing || self.state == .running else {
            preconditionFailure("Invalid task state")
        }
        if isRegistered {
            self.task?.suspend()
        } else {
            self.state = .suspended
        }
    }

    public override func cancel() {
        logDebug("ApiTask: will be cancelled")

        if isRegistered {
            self.task?.cancel()
        } else {
            self.state = self.state == .preparing ? .canceling : .completed
        }
    }

    // MARK: - Task registration process

    private func onMiddlewareResolved(withError error: Error?) {
        do {
            if let error = error { throw error }

            self.prepareTaskData()
        } catch {
            self.state = .completed
            self.cancel(withError: error)
        }
    }

    private func prepareTaskData() {
        if let transformer = self.requestTransformer, let data = self.bodyData {
            self.transformAsync(transformer: transformer, data: data) { result, error in
//                guard let strongSelf = self else { return }
                let strongSelf = self

                if let error = error {
                    strongSelf.state = .completed
                    strongSelf.cancel(withError: error)
                } else {
                    strongSelf.registerTask(withData: result)
                }
            }
        } else {
            self.registerTask(withData: nil)
        }
    }

    private func registerTask(withData data: Req.OutputType?) {
        // FIXME: Add Request and Response serializers that case input/output (respectively) to Data explicity
        self.delegate.registerApiTask(self, data: data as? Data)
        self.isRegistered = true

        self.task?.resume()
    }

    // MARK: - Data transformation

    internal func transformData() throws {
        if let response = self.response,
            let rawData = response.rawData as? Res.InputType {
            if self.responseTransformer.shouldTransform(data: rawData, sender: self) {
                self.outputData = try self.responseTransformer.transform(data: rawData, sender: self)
            }
        }
    }

    private func transformAsync<T: Transformer>(transformer: T, data: T.InputType, _ block: @escaping (T.OutputType?, Error?) -> Swift.Void) {
        let queue = DispatchQueue.global()

        queue.async { [weak self] in
            guard let strongSelf = self else { return }

            var outputData: T.OutputType?
            var error: Error?

            if transformer.shouldTransform(data: data, sender: strongSelf) {
                do {
                    outputData = try transformer.transform(data: data, sender: strongSelf)
                } catch let transformError {
                    error = transformError
                }
            }

            DispatchQueue.main.async {
                block(outputData, error)
            }
        }
    }
}
