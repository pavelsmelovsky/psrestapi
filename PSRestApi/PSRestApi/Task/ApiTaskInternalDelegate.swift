//
//  ApiTaskInternalDelegate.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 02.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

internal protocol ApiTaskInternalDelegate: class {
    func registerApiTask(_ apiTask: ApiTaskInternal, data: Data?)

    func apiTask(_ apiTask: ApiTaskInternal, resolveResponseMiddlewares completion: @escaping (URLSession.ResponseDisposition) -> Swift.Void)
    func apiTask(_ apiTask: ApiTaskInternal, resolveMiddlewares completion: @escaping ApiMiddlewareCallback)
}
