//
//  ApiRequest.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 15.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol ApiRequest: class {
    var endpoint: String { get }

    var scheme: String? { get set }
    var user: String? { get set }
    var password: String? { get set }
    var host: String? { get set }
    var port: Int? { get set }

    var httpMethod: ApiTaskMethod { get }
}
