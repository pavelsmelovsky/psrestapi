//
//  ApiRequestImpl.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 15.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public class ApiRequestImpl: ApiRequest {
    public private(set) var endpoint: String

    public var scheme: String?
    public var user: String?
    public var password: String?
    public var host: String?
    public var port: Int?

    public var httpMethod: ApiTaskMethod

    init(endpoint: String, httpMethod: ApiTaskMethod) {
        self.endpoint = endpoint
        self.httpMethod = httpMethod
    }
}
