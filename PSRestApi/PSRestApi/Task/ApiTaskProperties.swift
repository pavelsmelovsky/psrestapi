//
//  ApiTaskProperties.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol ApiTaskProperties: class {
    var task: URLSessionTask? { get }
    var state: ApiTaskState { get }

    var uploadProgress: Float { get }
    var downloadProgress: Float { get }

    var ignoreGlobalResponseMiddlewares: Bool { get set }

    var request: ApiRequest! { get }
    var response: ApiResponse? { get }

    func add(value: String, forQueryKey key: String)

    func set(value: String?, forHttpHeaderField headerName: String)
    func add(value: String, forHttpHeaderField headerName: String)

    func register(responseMiddleware: ApiResponseMiddleware)

    func resume()
    func cancel()
    func suspend()
}
