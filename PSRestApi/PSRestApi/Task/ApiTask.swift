//
//  ApiTask.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

open class ApiTask<Req: Transformer, Res: Transformer>: ApiTaskProperties, RequestHeadersContainer {
    public typealias ApiTaskSuccess = (_ data: Res.OutputType?, _ response: ApiResponse) -> Swift.Void
    public typealias ApiTaskFailure = (_ error: Error, _ task: ApiTask<Req, Res>) -> Swift.Void
    public typealias ApiTaskProgress = (_ progress: Float, _ task: ApiTask<Req, Res>) -> Swift.Void

    public internal(set) var task: URLSessionTask?
    public internal(set) var state: ApiTaskState = .suspended

    public internal(set) var request: ApiRequest!
    public internal(set) var response: ApiResponse?

    public var bodyData: Req.InputType?

    public private(set) var queryItems: [URLQueryItem] = []

    public var headers: [String: Any] = [:]

    public internal(set) var uploadProgress: Float = 0
    public internal(set) var downloadProgress: Float = 0

    public var ignoreGlobalResponseMiddlewares: Bool = false

    public var success: ApiTaskSuccess?
    public var failure: ApiTaskFailure?

    public var uploadProgressCallback: ApiTaskProgress?
    public var downloadProgressCallback: ApiTaskProgress?

    internal private(set) var requestTransformer: Req?
    internal private(set) var responseTransformer: Res

    internal private(set) var responseMiddlewares: [ApiResponseMiddleware] = []

    public internal(set) var outputData: Res.OutputType?

    public required init(apiRequest: ApiRequest, requestTransformer: Req?, responseTransformer: Res) {
        self.responseTransformer = responseTransformer

        self.request = apiRequest

        self.requestTransformer = requestTransformer
    }

    public func add(value: String, forQueryKey key: String) {
        let item = URLQueryItem(name: key, value: value)
        self.queryItems.append(item)
    }

    public func register(responseMiddleware: ApiResponseMiddleware) {
        self.responseMiddlewares.append(responseMiddleware)
    }

    open func resume() {
        fatalError("Must override")
    }

    open func cancel() {
        fatalError("Must override")
    }

    open func suspend() {
        fatalError("Must override")
    }
}
