//
//  PSRestApi.h
//  PSRestApi
//
//  Created by Pavel Smelovsky on 03.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PSRestApi.
FOUNDATION_EXPORT double PSRestApiVersionNumber;

//! Project version string for PSRestApi.
FOUNDATION_EXPORT const unsigned char PSRestApiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PSRestApi/PublicHeader.h>


