//
//  JSONSerializableTransformer.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 03.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public class JSONResponseTransformer<T>: Transformer {
    public typealias InputType = Data
    public typealias OutputType = T

    public required init() {}

    public func shouldTransform(data: Data, sender: Any) -> Bool {
        return !data.isEmpty
    }

    public func transform(data: Data, sender: Any) throws -> T {
        if let result = try JSONSerialization.jsonObject(with: data, options: []) as? T {
            return result
        }
        throw NSError.ApiSession.invalidResponseData
    }
}
