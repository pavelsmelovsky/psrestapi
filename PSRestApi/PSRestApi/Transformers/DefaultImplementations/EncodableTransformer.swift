//
//  EncodableTransformer.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 11.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

open class EncodableTransformer<In: Encodable>: Transformer {
    public typealias InputType = In
    public typealias OutputType = Data

    public var jsonEncoder = JSONEncoder()

    required public init() {

    }

    open func shouldTransform(data: In, sender: Any) -> Bool {
        return true
    }

    open func transform(data: InputType, sender: Any) throws -> OutputType {
        return try jsonEncoder.encode(data)
    }
}
