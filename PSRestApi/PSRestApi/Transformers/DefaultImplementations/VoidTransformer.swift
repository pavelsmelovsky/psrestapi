//
//  VoidTransformer.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 15.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public class VoidTransformer: Transformer {
    public typealias InputType = Swift.Void
    public typealias OutputType = Swift.Void

    required public init() {}
    public func shouldTransform(data: Void, sender: Any) -> Bool { return false }
    public func transform(data: Void, sender: Any) throws {}
}
