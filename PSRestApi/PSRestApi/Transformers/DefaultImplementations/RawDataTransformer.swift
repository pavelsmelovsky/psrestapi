//
//  RawDataTransformer.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 03.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public class RawDataTransformer: Transformer {
    public typealias InputType = Data
    public typealias OutputType = Data

    public required init() {}

    public func shouldTransform(data: Data, sender: Any) -> Bool {
        return true
    }

    open func transform(data: Data, sender: Any) throws -> Data {
        return data
    }
}
