//
//  DecodableTransformer.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

open class DecodableTransformer<T: Decodable>: Transformer {
    public typealias InputType = Data
    public typealias OutputType = T

    public var jsonDecoder = JSONDecoder()

    public required init() {

    }

    open func shouldTransform(data: Data, sender: Any) -> Bool {
        return !data.isEmpty
    }

    /// Default implementation decode object with generic type passed to instance of this class
    open func transform(data: InputType, sender: Any) throws -> OutputType {
        return try jsonDecoder.decode(OutputType.self, from: data)
    }
}
