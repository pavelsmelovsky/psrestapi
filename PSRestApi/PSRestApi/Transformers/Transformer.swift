//
//  Transformer.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol Transformer {
    associatedtype InputType
    associatedtype OutputType

    init()
    func shouldTransform(data: InputType, sender: Any) -> Bool
    func transform(data: InputType, sender: Any) throws -> OutputType
}
