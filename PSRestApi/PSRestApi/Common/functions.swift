//
//  functions.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 06.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

func assertMainThread(function: String, file: String, line: Int) {
    assert(Thread.isMainThread, "Function \(function) in file \(file):\(line) must be called in main thread")
}

func logVerbose(_ message: String) {
    #if VERBOSE_LOG
    debugPrint(message)
    #endif
}

func logDebug(_ message: String, function: StaticString = #function, file: StaticString = #file, line: UInt = #line) {
    #if DEBUG
    // ==> Debug: apiTask(_:) in /files/funcs.swift at line 123
    debugPrint("==> Debug: \(function) in \(file) at line \(line):")
    debugPrint(message)
    debugPrint("===")
    #endif
}

func logError(_ error: Error, function: StaticString = #function, file: StaticString = #file, line: UInt = #line) {
    #if DEBUG
    // ==> Debug: apiTask(_:) in /files/funcs.swift at line 123
    debugPrint("==> Error: \(function) in \(file) at line \(line):")
    debugPrint(error.localizedDescription)
    debugPrint("===")
    #endif
}
