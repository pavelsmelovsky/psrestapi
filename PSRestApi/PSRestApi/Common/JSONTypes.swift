//
//  RSDefines.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 06.03.16.
//  Copyright © 2016 Smelovsky.Me. All rights reserved.
//

#if os(OSX)
    import Cocoa
#elseif os(iOS)
    import Foundation
#endif

// JSON typealias

public typealias JSArray                 = [Any]
public typealias JSObject                = [String: Any]
public typealias JSObjectArray           = [JSObject]

public typealias JSIntArray              = [Int]
public typealias JSDoubleArray           = [Double]
public typealias JSFloatArray            = [Float]
public typealias JSBoolArray             = [Bool]
public typealias JSStringArray           = [String]

public func doubleValue(_ value: Any?) -> Double {
    if let value = value as? Double {
        return value
    } else if let value = value as? String,
        let dValue = Double(value) {
        return dValue
    }
    return Double(0)
}
