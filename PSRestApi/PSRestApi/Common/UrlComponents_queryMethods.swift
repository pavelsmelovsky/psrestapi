//
//  UrlComponents_queryMethods.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 24.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#if os(OSX)
    import Cocoa
#elseif os(iOS)
    import Foundation
#endif

extension URLComponents {
    mutating func addQueryValue(_ value: String, forKey key: String) {
        let queryItem = URLQueryItem(name: key, value: value)

        if self.queryItems == nil {
            self.queryItems = []
        }

        self.queryItems?.append(queryItem)
    }

    mutating func addAllQueryValues(_ values: [String: String]) {
        for value in values {
            self.addQueryValue(value.value, forKey: value.key)
        }
    }

    mutating func setQueryValue(_ value: String, forKey key: String) {
        self.removeQueryKey(key)
        self.addQueryValue(value, forKey: key)
    }

    mutating func setAllQueryValues(_ values: [String: String]) {
        self.queryItems = self.queryItems?.filter { return !values.keys.contains($0.name) }
        self.addAllQueryValues(values)
    }

    mutating func removeQueryKey(_ key: String) {
        self.queryItems = self.queryItems?.filter {return $0.name != key}
    }
}
