//
//  NSError+Response.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 16.08.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#if os(OSX)
    import Cocoa
#elseif os(iOS)
    import Foundation
#endif

private let NSResponseErrorCodeKey = "response.errorCode"
private let NSResponseErrorMessageKey = "response.errorMessage"

extension NSError {
    convenience init(domain: String, response: HTTPURLResponse, attrs: JSObject) {
        let code = response.statusCode

        let errCode = attrs["errorCode"] as? String ?? "unknown_error"
        let errMessage = attrs["errorMessage"] as? String ?? "Unknown error"

        let localizedMessage = Bundle.main.localizedString(forKey: errCode, value: errMessage, table: "ApiResponse")

        let errInfo = [NSResponseErrorCodeKey: errCode,
                       NSResponseErrorMessageKey: errMessage,
                       NSLocalizedDescriptionKey: localizedMessage]

        self.init(domain: domain, code: code, userInfo: errInfo)
    }
}
