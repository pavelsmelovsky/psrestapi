//
//  URLComponents+BuildUrl.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 17.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

extension URLComponents {
    internal init?(apiTask: ApiTaskInternal, resolvingAgainstBaseURL baseUrl: URL) {
        var baseUrl = baseUrl

        if !baseUrl.hasDirectoryPath {
            baseUrl.appendPathComponent("/", isDirectory: false)
        }

        var endpoint = apiTask.request.endpoint
        if endpoint.first == "/" {
            endpoint.removeFirst()
        }

        guard let url = URL(string: endpoint, relativeTo: baseUrl) else {
            return nil
        }

        self.init(url: url, resolvingAgainstBaseURL: true)
        self.commonSetup(apiTask)
    }

    private mutating func commonSetup(_ apiTask: ApiTaskInternal) {
        if !apiTask.queryItems.isEmpty {
            var queryItems = self.queryItems ?? []
            queryItems.append(contentsOf: apiTask.queryItems)
            self.queryItems = queryItems
        }

        if let scheme = apiTask.request.scheme {
            self.scheme = scheme
        }

        if let user = apiTask.request.user {
            self.user = user
        }

        if let password = apiTask.request.password {
            self.password = password
        }

        if let host = apiTask.request.host {
            self.host = host
        }

        if let port = apiTask.request.port {
            self.port = port
        }
    }
}
