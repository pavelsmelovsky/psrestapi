//
//  ApiStatusCodes.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 24.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#if os(OSX)
    import Cocoa
#elseif os(iOS)
    import Foundation
#endif

public enum ApiStatusCode: Int {
    case unknown                    = 0

    case `continue`                 = 100
    case switchingProtocol          = 101
    case processing                 = 102

    // MARK: - Success statuses

    case ok                         = 200
    case created                    = 201
    case accepted                   = 202
    case notAuthoritative           = 203
    case noContent                  = 204
    case resetContent               = 205
    case partialContent             = 206

    // MARK: - Redirect statuses

    case notModified                = 304

    // MARK: - Client statuses

    case badRequest                 = 400
    case unauthorized               = 401
    case paymentRequired            = 402
    case forbidden                  = 403
    case notFound                   = 404
    case notAllowed                 = 405
    case notAcceptable              = 406
    case proxyAuthRequired          = 407
    case timedOut                   = 408
    case conflict                   = 409
    case gone                       = 410
    case lengthRequired             = 411
    case preconditionFailure        = 412
}
