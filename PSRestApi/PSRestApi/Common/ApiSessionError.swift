//
//  ApiSessionError.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 03.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

// swiftlint:disable identifier_name
public let ApiSessionErrorDomain = "me.smelovsky.ApiSession.Error"
private let ApiSessionErrorLocalizeTable = "ApiSessionErrors"
// swiftlint:enable identifier_name

public enum ApiSessionError: Int {
    case unknownError = 10_000
    case invalidResponse = 10_001
    case invalidResponseData = 10_010
}

public extension NSError {
    private typealias LocalizeTuple = (key: String, value: String)

    struct ApiSession {
        public static var invalidResponse: NSError {
            let description = (key: "Error.InvalidResponse.Description", value: "Invalid response")
            return sessionError(code: .invalidResponse, description: description, reason: nil)
        }

        public static var invalidResponseData: NSError {
            let description = (key: "Error.InvalidResponseData.Description", value: "Invalid response data")
            return sessionError(code: .invalidResponseData, description: description, reason: nil)
        }
    }

    private static func sessionError(code: ApiSessionError, description: LocalizeTuple, reason: LocalizeTuple?) -> Self {
        let bundle = Bundle.main

        var errorInfo: [String: Any] = [:]

        let localizedDescription = bundle.localizedString(forKey: description.key, value: description.value, table: ApiSessionErrorLocalizeTable)
        errorInfo[NSLocalizedDescriptionKey] = localizedDescription

        if let reason = reason {
            let localizedReason = bundle.localizedString(forKey: reason.key, value: reason.value, table: ApiSessionErrorLocalizeTable)
            errorInfo[NSLocalizedFailureReasonErrorKey] = localizedReason
        }

        return self.init(domain: ApiSessionErrorDomain, code: code.rawValue, userInfo: errorInfo)
    }
}
