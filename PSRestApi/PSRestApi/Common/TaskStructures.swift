//
//  Structures.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 04.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public enum ApiTaskMethod: String {
    case get        = "GET"
    case post       = "POST"
    case put        = "PUT"
    case patch      = "PATCH"
    case delete     = "DELETE"
}

public enum ApiTaskState: Int {
    case suspended      = 0
    case preparing      = 1
    case running        = 2
    case canceling      = 3
    case completed      = 4

    init?(sessionState: URLSessionTask.State) {
        let rawValue: Int
        switch sessionState {
        case .suspended: rawValue = 0
        case .running: rawValue = 2
        case .canceling: rawValue = 3
        case .completed: rawValue = 4
        @unknown default: fatalError("Unknown session state")
        }
        self.init(rawValue: rawValue)
    }
}
