//
//  ApiResponseMiddleware.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 06/10/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol ApiResponseMiddleware {
    /// Method called when session task receive response
    ///
    /// - Parameter response: ApiResponse object
    /// - Returns: ResponseDisposition
    func receive(response: ApiResponse) -> URLSession.ResponseDisposition
}
