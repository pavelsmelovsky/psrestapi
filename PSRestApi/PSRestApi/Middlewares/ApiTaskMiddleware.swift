//
//  ApiTaskMiddleware.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 28.07.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public typealias ApiMiddlewareCallback = (_ error: Error?) -> Swift.Void

public protocol ApiTaskMiddleware {
    func shouldPerform(forTask task: ApiTaskProperties) -> Bool
    func setup(task: ApiTaskProperties, completion: @escaping ApiMiddlewareCallback)
}
