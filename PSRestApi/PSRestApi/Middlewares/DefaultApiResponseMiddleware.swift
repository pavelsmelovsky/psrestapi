//
//  DefaultApiResponseMiddleware.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 07/10/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public struct DefaultApiResponseMiddleware: ApiResponseMiddleware {
    typealias BlockType = (ApiResponse) -> URLSession.ResponseDisposition

    private var block: BlockType

    init(_ block: @escaping BlockType) {
        self.block = block
    }

    public func receive(response: ApiResponse) -> URLSession.ResponseDisposition {
        return self.block(response)
    }
}
