//
//  URLRequest+ApiRequest.swift
//  PSRestApi
//
//  Created by Pavel Smelovsky on 07/10/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

extension URLRequest {
    init(session: ApiSession, apiTask: ApiTaskInternal, data: Data?) {
        let components = URLComponents(apiTask: apiTask, resolvingAgainstBaseURL: session.url)
        guard let url = components?.url else { fatalError() }

        self.init(url: url)

        if let data = data {
            self.httpBody = data
        }

        self.append(headers: session.headers)
        self.append(headers: apiTask.headers)

        self.httpMethod = apiTask.request.httpMethod.rawValue
    }

    private mutating func append(headers: [String: Any]) {
        for item in headers {
            if let value = item.value as? String {
                self.setValue(value, forHTTPHeaderField: item.key)
            } else if var values = item.value as? [String], !values.isEmpty {
                let first = values.removeFirst()
                self.setValue(first, forHTTPHeaderField: item.key)
                for value in values {
                    self.addValue(value, forHTTPHeaderField: item.key)
                }
            }
        }
    }
}
