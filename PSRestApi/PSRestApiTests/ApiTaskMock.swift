//
//  ApiTaskMock.swift
//  PSRestApiTests
//
//  Created by Pavel Smelovsky on 07/10/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation
@testable import PSRestApi

class ApiTaskMock: ApiTaskInternal {
    var task: URLSessionTask?
    var state: ApiTaskState = .suspended
    var uploadProgress: Float = 0
    var downloadProgress: Float = 0

    var queryItems: [URLQueryItem] = []

    var responseMiddlewares: [ApiResponseMiddleware] = []

    var ignoreGlobalResponseMiddlewares: Bool = false

    var request: ApiRequest!
    var response: ApiResponse?

    var headers: [String: Any] = [:]

    init(endpoint: String, httpMethod: ApiTaskMethod = .get) {
        self.request = ApiRequestImpl(endpoint: endpoint, httpMethod: httpMethod)
    }

    // MARK: - Methods

    func register(responseMiddleware: ApiResponseMiddleware) {
        fatalError()
    }

    func resume() {
        fatalError()
    }

    func cancel() {
        fatalError()
    }

    func suspend() {
        fatalError()
    }

    // MARK: - Query items

    func add(value: String, forQueryKey key: String) {
        let item = URLQueryItem(name: key, value: value)
        self.queryItems.append(item)
    }

    // MARK: - ApiSessionDelegate

    func apiSession(_ apiSession: ApiSession, didReceiveResponse response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        fatalError()
    }

    func apiSession(_ apiSession: ApiSession, didReceiveData data: Data) {
        fatalError()
    }

    func apiSessionLoadingCompleted(_ apiSession: ApiSession) throws {
        fatalError()
    }

    func apiSession(_ apiSession: ApiSession, didCompleteWithError error: Error?) {
        fatalError()
    }
}
