//
//  UrlBuildingTest.swift
//  PSRestApiTests
//
//  Created by Pavel Smelovsky on 11.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest
@testable import PSRestApi
//import PSRestApi

class TestMiddleware: ApiTaskMiddleware {
    func shouldPerform(forTask task: ApiTaskProperties) -> Bool {
        return task.request.endpoint == "/me"
    }

    func setup(task: ApiTaskProperties, completion: @escaping ApiMiddlewareCallback) {
        task.add(value: "Bearer Token", forHttpHeaderField: "Authorization")
        completion(nil)
    }
}

class UrlBuildingTest: XCTestCase {
    var session: RestApiSession!

    override func setUp() {
        super.setUp()
        if session == nil {
            session = RestApiSession(url: URL(string: "https://localhost:8080")!)

            session.set(value: "value1", forHttpHeaderField: "field1")
            session.set(value: "value2.1", forHttpHeaderField: "field2")
            session.add(value: "value2.2", forHttpHeaderField: "field2")
            session.add(value: "value2.3", forHttpHeaderField: "field2")

            session.register(middleware: TestMiddleware())
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testUrl() {
        let transformer = ApiDataTransformator<User>()
        let task = session.read(endpoint: "/test/path?customKey1=customValue1&customKey2=customValue2", responseTransformer: transformer) as? ApiTaskInternalImpl
        XCTAssertNotNil(task)

        task?.add(value: "value1", forQueryKey: "key1")
        task?.add(value: "value2", forQueryKey: "key2")

        task?.add(value: "task-value1", forHttpHeaderField: "task-field-1")
        task?.add(value: "task-value2", forHttpHeaderField: "task-field-2")
        task?.add(value: "task-value3", forHttpHeaderField: "task-field-2")

        let req1 = URLRequest(session: session, apiTask: task!, data: nil)
        XCTAssertEqual(req1.url?.absoluteString, "https://localhost:8080/test/path?customKey1=customValue1&customKey2=customValue2&key1=value1&key2=value2")

        verifyHeaders(req1.allHTTPHeaderFields!)

        task?.request.host = "myhost"

        let req2 = URLRequest(session: session, apiTask: task!, data: nil)
        XCTAssertEqual(req2.url?.absoluteString, "https://myhost:8080/test/path?customKey1=customValue1&customKey2=customValue2&key1=value1&key2=value2")

        verifyHeaders(req2.allHTTPHeaderFields!)

        task?.request.port = 1001
        task?.request.scheme = "ftp"
        task?.request.user = "username"
        task?.request.password = "userpwd"

        let req3 = URLRequest(session: session, apiTask: task!, data: nil)
        XCTAssertEqual(req3.url?.absoluteString, "ftp://username:userpwd@myhost:1001/test/path?customKey1=customValue1&customKey2=customValue2&key1=value1&key2=value2")

        verifyHeaders(req3.allHTTPHeaderFields!)

        XCTAssert(session.allTasks().isEmpty, "All tasks must be zero")
    }

    func verifyHeaders(_ headers: [String: String]) {
        XCTAssertEqual(headers.count, 4)
        XCTAssertEqual(headers["field1"], "value1")
        XCTAssertEqual(headers["field2"], "value2.1,value2.2,value2.3")
        XCTAssertEqual(headers["task-field-1"], "task-value1")
        XCTAssertEqual(headers["task-field-2"], "task-value2,task-value3")
    }

    func testHeadersAndQuery() {
        let exp = expectation(description: "testheadersandquery")

        let task = session.read(endpoint: "/me", responseTransformer: ApiDataTransformator<User>()) as? ApiTaskInternalImpl
        XCTAssertNotNil(task)

        task?.failure = { (error, _) in
            logError(error)

            XCTAssert(self.session.allTasks().isEmpty, "All tasks must be zero")
            exp.fulfill()
        }

        let req1 = URLRequest(session: session, apiTask: task!, data: nil)
        XCTAssertEqual(req1.url?.absoluteString, "https://localhost:8080/me")

        task?.add(value: "value", forQueryKey: "key")

        let req2 = URLRequest(session: session, apiTask: task!, data: nil)
        XCTAssertEqual(req2.url?.absoluteString, "https://localhost:8080/me?key=value")

        let allHeaders2 = req2.allHTTPHeaderFields!
        XCTAssertEqual(allHeaders2.count, 2)

        task?.add(value: "value", forHttpHeaderField: "name")

        var req3 = URLRequest(session: session, apiTask: task!, data: nil)
        XCTAssertNotNil(req3.url)
        let allHeaders3 = req3.allHTTPHeaderFields!
        XCTAssertEqual(allHeaders3.count, 3)

        task?.resume()

        let req4 = URLRequest(session: session, apiTask: task!, data: nil)
        XCTAssertNotNil(req4.url)
        XCTAssertEqual(req3.url, req4.url)

        let allHeaders4 = req4.allHTTPHeaderFields!
        XCTAssertEqual(allHeaders4.count, 4) // add header in middleware

        let authHeader = req4.value(forHTTPHeaderField: "Authorization")
        XCTAssertNotNil(authHeader)

        if let value = authHeader {
            XCTAssertEqual(value, "Bearer Token")
        }

        task?.cancel()

        XCTAssert(session.allTasks().count == 1, "Task count must be equal to 1")

        waitForExpectations(timeout: 1.0) { (error) in
            XCTAssertNil(error)

            if let error = error {
                logError(error)
            }
        }
    }

    func testRemovingFromSession() {
        self.removingTaskFromSession(onCancel: false)
    }

    func testRemovingOnCancelFromSession() {
        self.removingTaskFromSession(onCancel: true)
    }

    func removingTaskFromSession(onCancel cancel: Bool) {
        let exp = expectation(description: "Test remove from session")

        let task = session.read(endpoint: "/me", responseTransformer: ApiDataTransformator<User>()) as? ApiTaskInternalImpl
        XCTAssertNotNil(task)

        task?.failure = { (error, _) in
            logError(error)

            XCTAssert(self.session.allTasks().isEmpty, "All tasks must be removed from session")

            exp.fulfill()
        }

        XCTAssert(session.allTasks().isEmpty, "Number of tasks must be zero")
        let tasks = session.allTasks()
        for task in tasks {
            logVerbose("Active task \(task.request.endpoint)")
        }

        task?.resume()

        // because our middleware is sync, state must be running
        XCTAssertEqual(task?.state, .running)

        XCTAssertEqual(session.allTasks().count, 1)

        if cancel {
            task?.cancel()
        }

        waitForExpectations(timeout: 5.0) { (error) in
            XCTAssertNil(error)

            if let error = error {
                logError(error)
            }
        }
    }

    // MARK: - Other tests

    typealias UrlTuple = (baseUrl: String, endpoint: String, result: String)

    func testComponents() {
        var items: [UrlTuple] = []

        items.append((baseUrl: "https://localhost", endpoint: "/tasks", result: "https://localhost/tasks"))
        items.append((baseUrl: "https://localhost", endpoint: "/tasks/", result: "https://localhost/tasks/"))
        items.append((baseUrl: "https://localhost", endpoint: "tasks", result: "https://localhost/tasks"))
        items.append((baseUrl: "https://localhost", endpoint: "tasks/", result: "https://localhost/tasks/"))
        //
        items.append((baseUrl: "https://localhost/", endpoint: "/tasks", result: "https://localhost/tasks"))
        items.append((baseUrl: "https://localhost/", endpoint: "/tasks/", result: "https://localhost/tasks/"))
        items.append((baseUrl: "https://localhost/", endpoint: "tasks", result: "https://localhost/tasks"))
        items.append((baseUrl: "https://localhost/", endpoint: "tasks/", result: "https://localhost/tasks/"))

        items.append((baseUrl: "https://localhost/api/v1", endpoint: "/tasks", result: "https://localhost/api/v1/tasks"))
        items.append((baseUrl: "https://localhost/api/v1", endpoint: "/tasks/", result: "https://localhost/api/v1/tasks/"))
        items.append((baseUrl: "https://localhost/api/v1", endpoint: "tasks", result: "https://localhost/api/v1/tasks"))
        items.append((baseUrl: "https://localhost/api/v1", endpoint: "tasks/", result: "https://localhost/api/v1/tasks/"))

        items.append((baseUrl: "https://localhost/api/v1/", endpoint: "/tasks", result: "https://localhost/api/v1/tasks"))
        items.append((baseUrl: "https://localhost/api/v1/", endpoint: "/tasks/", result: "https://localhost/api/v1/tasks/"))
        items.append((baseUrl: "https://localhost/api/v1/", endpoint: "tasks", result: "https://localhost/api/v1/tasks"))
        items.append((baseUrl: "https://localhost/api/v1/", endpoint: "tasks/", result: "https://localhost/api/v1/tasks/"))

        for item in items {
            guard let url = URL(string: item.baseUrl) else {
                XCTFail("Cannot create url from \(item.baseUrl)")
                continue
            }

            let task = ApiTaskMock(endpoint: item.endpoint)
            guard let components = URLComponents(apiTask: task, resolvingAgainstBaseURL: url) else {
                XCTFail("Cannot create url components from url \(url.description)")
                continue
            }

            XCTAssertEqual(components.url?.absoluteString, item.result, "\(item.baseUrl) + \(item.endpoint)")
        }
    }
}
