//
//  PSRestApiTests.swift
//  PSRestApiTests
//
//  Created by Pavel Smelovsky on 03.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest
@testable import PSRestApi
//import PSRestApi

extension Dictionary {
    static public func == (lhs: [AnyHashable: Any], rhs: [AnyHashable: Any] ) -> Bool {
        return NSDictionary(dictionary: lhs).isEqual(to: rhs)
    }
}

class PSRestApiTests: XCTestCase {
    var session: RestApiSession!

    override func setUp() {
        super.setUp()

        // Put setup code here. This method is called before the invocation of each test method in the class.

        if session == nil, let url = URL(string: "https://localhost") {
            session = RestApiSession(url: url)
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDecode() {
        let json = """
{"status":1,"data":{"id":123,"firstName":"My Name","lastName":"Lastname"},"metadata":{"type":4,"message":"hello world","double":1.444,"bool":true,"int":1}}
"""
        guard let data = json.data(using: .utf8) else { fatalError() }
        guard let apiData = try? JSONDecoder().decode(ApiData<User>.self, from: data) else { fatalError() }

        XCTAssertEqual(apiData.status, 1)

        guard let user = apiData.data else {
            XCTFail("data is nil"); return
        }

        XCTAssertEqual(user.id, 123)
        XCTAssertEqual(user.firstName, "My Name")

//        guard let metadata = apiData.metadata else {
//            XCTFail("Metadata is nil"); return
//        }
//
//        if let type = metadata["type"] as? Int {
//            XCTAssertEqual(type, 4)
//        } else {
//            XCTFail("Type value does not exists in metadata")
//        }
//
//        if let message = metadata["message"] as? String {
//            XCTAssertEqual(message, "hello world")
//        } else {
//            XCTFail("Message value does not exists in metadata")
//        }
//
//        if let dbl = metadata["double"] as? Double {
//            XCTAssertEqual(dbl, 1.444)
//        } else {
//            XCTFail("Double value does not exists in metadata")
//        }
//
//        if let bl = metadata["bool"] as? Bool {
//            XCTAssertEqual(bl, true)
//        } else {
//            XCTFail("Bool value does not exists in metadata")
//        }
//
//        if let int = metadata["int"] as? Int {
//            XCTAssertEqual(int, 1)
//        } else {
//            XCTFail("Int value does not exists in metadata")
//        }
    }

    func testDecodable() {
        let transformer = DecodableTransformer<User>()

        let task = session.read(endpoint: "user", responseTransformer: transformer)
        task.success = { (data, response) in
            logVerbose("\(data.debugDescription)")
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
