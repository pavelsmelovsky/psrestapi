//
//  ApiTaskStates.swift
//  PSRestApiTests
//
//  Created by Pavel Smelovsky on 08.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest

@testable import PSRestApi

enum MiddlewareError: Error {
    case cancelled
}

class SyncMiddleware: ApiTaskMiddleware {
    func shouldPerform(forTask task: ApiTaskProperties) -> Bool {
        return task.request.endpoint == "/user"
    }

    func setup(task: ApiTaskProperties, completion: @escaping ApiMiddlewareCallback) {
        completion(nil)
    }
}

class SyncCancellableMiddleware: ApiTaskMiddleware {
    func shouldPerform(forTask task: ApiTaskProperties) -> Bool {
        return task.request.endpoint == "/users"
    }

    func setup(task: ApiTaskProperties, completion: @escaping ApiMiddlewareCallback) {
        completion(MiddlewareError.cancelled)
    }
}

class AsyncMiddleware: ApiTaskMiddleware {
    func shouldPerform(forTask task: ApiTaskProperties) -> Bool {
        return task.request.endpoint == "/users/async"
    }

    func setup(task: ApiTaskProperties, completion: @escaping ApiMiddlewareCallback) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
            completion(nil)
        }
    }
}

class ApiTaskStates: XCTestCase {
    var session: RestApiSession!

    private func commonInit() {
        let sync = SyncMiddleware()
        let syncCancelled = SyncCancellableMiddleware()
        let async = AsyncMiddleware()

        guard let url = URL.init(string: "http://localhost:3000") else {
            fatalError()
        }

        session = RestApiSession(url: url)
        session.register(middleware: sync)
        session.register(middleware: async)
        session.register(middleware: syncCancelled)

        session.add(value: "application/json", forHttpHeaderField: "Content-Type")
    }

    override func setUp() {
        super.setUp()

        if session == nil {
            self.commonInit()
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSync() {
        let expectation = self.expectation(description: "testSync")

        self.performSync(expectation)

        self.waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    private func performSync(_ expectation: XCTestExpectation) {
        let transformer = ApiDataTransformator<User>()
        let task = self.session.read(endpoint: "/user", responseTransformer: transformer)

        task.success = { (user, response) in
            XCTAssertNotNil(user)
            expectation.fulfill()
        }

        task.failure = { (error, _) in
            XCTAssertNil(error)
            expectation.fulfill()
        }

        XCTAssertEqual(task.state, .suspended)
        task.resume()
        XCTAssertEqual(task.state, .running)
    }

    func testSyncAndCancel() {
        let expectation = self.expectation(description: "testSyncAndCancel")

        let transformer = ApiDataTransformator<User>()
        let task = session.read(endpoint: "/user", responseTransformer: transformer)
        task.success = { (user, response) in
            XCTFail("This task must be cancelled")
            expectation.fulfill()
        }

        task.failure = { (error, task) in
            let nsError = error as NSError
            XCTAssertEqual(nsError.code, NSURLErrorCancelled)
            expectation.fulfill()
        }

        XCTAssertEqual(task.state, .suspended)
        task.resume()
        XCTAssertEqual(task.state, .running)
        task.cancel()

        self.waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testSyncWithCancelledMiddleware() {
        let transformer = ApiDataTransformator<User>()
        let task = session.read(endpoint: "/users", responseTransformer: transformer)
        task.success = { (user, response) in

        }

        task.failure = { (error, task) in

        }

        XCTAssertEqual(task.state, .suspended)
        task.resume()
        XCTAssertEqual(task.state, .completed)
    }

    func testSyncMiddleware() {
        syncRequest(endpoint: "/user", type: User.self)
    }

    func testNoMiddlewares() {
        syncRequest(endpoint: "/users/nomiddlewares", type: [User].self)
    }

    func syncRequest<T: Codable>(endpoint: String, type: T.Type) {
        let expectation = self.expectation(description: "wait server response")

        let transformer = ApiDataTransformator<T>()
        let task = session.read(endpoint: endpoint, responseTransformer: transformer)
        task.success = { (user, response) in
            XCTAssertEqual(task.state, .completed)
            XCTAssertNotNil(user, "User cannot be nil")

            logVerbose(user.debugDescription)

            expectation.fulfill()
        }

        task.failure = { (error, task) in
            XCTFail(error.localizedDescription)
            expectation.fulfill()
        }

        XCTAssertEqual(task.state, .suspended)
        task.resume()
        XCTAssertEqual(task.state, .running)

        self.waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testAsyncMiddleware() {
        self.asyncMiddleware(cancelled: false)
    }

    func testCancelledAsyncMiddleware() {
        self.asyncMiddleware(cancelled: true)
    }

    func asyncMiddleware(cancelled: Bool) {
        let expectation = self.expectation(description: "wait server response")

        let transformer = ApiDataTransformator<[User]>()
        let task = session.read(endpoint: "/users/async", responseTransformer: transformer)
        task.success = { [unowned task] (users, response) in
            XCTAssertEqual(task.state, .completed)

            if cancelled {
                XCTAssertNil(users, "Task is cancelled, users cannot be valid object")
                XCTFail("This oepration is cancelled")
            } else {
                XCTAssertNotNil(users, "User cannot be nil")
                logVerbose(users.debugDescription)
            }

            expectation.fulfill()
        }

        task.failure = { (error, task) in
            if !cancelled {
                XCTFail(error.localizedDescription)
            } else {
                let nsError = error as NSError
                XCTAssertEqual(nsError.code, NSURLErrorCancelled)
            }
            expectation.fulfill()
        }

        XCTAssertEqual(task.state, .suspended)
        task.resume()
        XCTAssertEqual(task.state, .preparing)

        if cancelled {
            // cancel after 3 seconds (while AsyncMiddleware running
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: { [unowned task] in
                XCTAssertEqual(task.state, .preparing)
                task.cancel()
            })
        }

        self.waitForExpectations(timeout: 8.0) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }

    func testAsync() {
        let expectation = self.expectation(description: "Some description")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
            expectation.fulfill()
        }

        self.waitForExpectations(timeout: 6.0) { (error) in
            XCTAssertNil(error, "Expectation error: \(error.debugDescription)")
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testPostRequest() {
        let expectation = self.expectation(description: "testPostRequest")

        self.performPostRequest(expectation)

        wait(for: [expectation], timeout: 5.0)
    }

    private func performPostRequest(_ expectation: XCTestExpectation) {
        let task = session.create(endpoint: "/user/name", data: UserName(name: "MyName"), requestTransformer: EncodableTransformer<UserName>(), responseTransformer: DecodableTransformer<ApiData<UserName>>())
        task.success = { [weak task] result, response in
            XCTAssertNotNil(result)
            XCTAssertEqual(result?.data?.name, "MyName")

            DispatchQueue.main.async { [weak task] in
                XCTAssertNil(task)
                expectation.fulfill()
            }
        }
        task.failure = { error, _ in
            XCTFail(error.localizedDescription)
            expectation.fulfill()
        }
        task.resume()
    }
}

struct UserName: Codable {
    var name: String
}
