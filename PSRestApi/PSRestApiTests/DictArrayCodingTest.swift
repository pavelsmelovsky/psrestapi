//
//  DictArrayCodingTest.swift
//  PSRestApiTests
//
//  Created by Pavel Smelovsky on 30.03.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import XCTest

class DictArrayCodingTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    // swiftlint:disable function_body_length
    func testExample() {
        let json: [String: Any] = ["key1": "string1",
                                   "key2": 123,
                                   "key3": true]
        let objects: [Any] = [321, "Hello world", true, Float(-123.3)]

        let testObject = TestObject(index: 5, json: json, objects: objects)

        do {
            let data = try JSONEncoder().encode(testObject)
            let decodedObject = try JSONDecoder().decode(TestObject.self, from: data)

            XCTAssertEqual(testObject.index, decodedObject.index)

            XCTAssertEqual(testObject.json.count, decodedObject.json.count)

            let key1 = "key1"
            XCTAssertNotNil(testObject.json[key1] as? String)
            XCTAssertNotNil(decodedObject.json[key1] as? String)
            XCTAssertEqual(testObject.json[key1] as? String, decodedObject.json[key1] as? String)

            let key2 = "key2"
            XCTAssertNotNil(testObject.json[key2] as? Int)
            XCTAssertNotNil(decodedObject.json[key2] as? Int)
            XCTAssertEqual(testObject.json[key2] as? Int, decodedObject.json[key2] as? Int)

            let key3 = "key3"
            XCTAssertNotNil(testObject.json[key3] as? Bool)
            XCTAssertNotNil(decodedObject.json[key3] as? Bool)
            XCTAssertEqual(testObject.json[key3] as? Bool, decodedObject.json[key3] as? Bool)

            let array1 = testObject.objects
            let array2 = decodedObject.objects

            var idx = 0
            XCTAssertNotNil(array1[idx] as? Int)
            XCTAssertNotNil(array2[idx] as? Int)
            XCTAssertEqual(array1[idx] as? Int, array2[idx] as? Int)

            idx += 1
            XCTAssertNotNil(array1[idx] as? String)
            XCTAssertNotNil(array2[idx] as? String)
            XCTAssertEqual(array1[idx] as? String, array2[idx] as? String)

            idx += 1
            XCTAssertNotNil(array1[idx] as? Bool)
            XCTAssertNotNil(array2[idx] as? Bool)
            XCTAssertEqual(array1[idx] as? Bool, array2[idx] as? Bool)

            idx += 1
            XCTAssertNotNil(array1[idx] as? Float)
            XCTAssertNotNil(array2[idx] as? Float)
            XCTAssertEqual(array1[idx] as? Float, array2[idx] as? Float)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    // swiftlint:enable function_body_length

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}

struct TestObject: Codable {
    var index: Int
    var json: [String: Any]
    var objects: [Any]

    enum CodingKeys: String, CodingKey {
        case index
        case json
        case objects
    }

    init(index: Int, json: [String: Any], objects: [Any]) {
        self.index = index
        self.json = json
        self.objects = objects
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        index = try container.decode(Int.self, forKey: .index)
        json = try container.decode([String: Any].self, forKey: .json)
        objects = try container.decode([Any].self, forKey: .objects)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(index, forKey: .index)
        try container.encode(json, forKey: .json)
        try container.encode(objects, forKey: .objects)
    }
}
