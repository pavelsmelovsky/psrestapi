//
//  Models.swift
//  PSRestApiTests
//
//  Created by Pavel Smelovsky on 08.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import PSRestApi

struct User: Codable, CustomDebugStringConvertible {
    var id: Int
    var firstName: String
    var lastName: String

    var debugDescription: String {
        return ["id": id,
                "firstName": firstName,
                "lastName": lastName].debugDescription
    }
}

struct ApiError: Codable, Error {
    var code: String
    var message: String
}

struct ApiData<T: Codable>: Codable {
    var status: Int
    var data: T?
    var error: ApiError?
}

class ApiDataTransformator<T: Codable>: DecodableTransformer<T> {
    required init() {
        super.init()
    }

    override func transform(data: DecodableTransformer<T>.InputType, sender: Any) throws -> T {
        let res = try self.jsonDecoder.decode(ApiData<T>.self, from: data)

        if let error = res.error {
            throw error
        } else if let data = res.data {
            return data
        }
        fatalError()
    }
}
